/*
 * Total pollution calculation : solar or nuclear? 
 * gcc -Wall pollution.c -o pollution -lm
 */
 
#include <stdio.h>
#include <math.h> // fmaxf()

const float SOLAR_PANEL_RATIO  = 1.428; // Charging accumulators consume power, need more solar panels for that.
const float SOLAR_PANEL_POWER  = 0.06;  // Solar panel power in MW
const float ACCUMULATOR_RATIO  = 0.84;  // Accumulators per solar panels.
const float STEAM_ENGINE_POWER = 0.9;   // Steam engine power in MW
const float ENGINE_PER_BOILER  = 2;     // One boiler can feed 2 engines.
const float BOILER_POLLUTION   = 30;    // Pollution per minute.
const float OIL_LIGHT_RATIO    = 0.85;  // How many light2petrol plants per refinery.
const float OIL_HEAVY_RATIO    = 0.25;  // How many heavy2light plants per refinery.
const float OIL_PETROL_AMOUNT  = 1170;  // The petrol production per minute of one advanced oil refinery with plants according to kirkmcdonald.github.io
const float STEAM_RATIO        = 1.716; // 103/60, exchangers steam output divided by turbines steam input.
const float HEATPIPE_RATIO     = 1.45;  // Heatpipes on exchangers limit for 2, 4, 6, etc. reactors. 
const float EXCHANGER_POWER    = 10;    // Exchanger heat consumption.

// Global variables of ComputeMinimalReactorSetup function
int reactor;      // Number of reactors for target power.
float edge;       // Edges between reactor stacks, a 2x2 has 4 edges, a 3x2 has 7 edges, etc.
float exchanger;  // Exchangers can be computed from edges.
float turbine;    // Turbines can be computed from exchangers.
float power;      // Power can be computed from exchangers.
float heatpipe;   // Heatpipes can be computed from exchangers.
 
struct building {
    float craftspeed; // Crafting speed of the building
    int pollution;    // Pollution per minute of the building
    float power;      // Electrical power in MW of the building
};

struct building assembler =  {0.75, 3,  0.155};
struct building furnace =    {2,    4,  0.000};
struct building plant =      {1,    4,  0.217};
struct building refinery =   {1,    6,  0.434};
struct building miner =      {0.5,  10, 0.090};
struct building centrifuge = {1,    4,  0.362};

/*
 * The total pollution of researching red science.
 */
float ComputeRedPollution(int total_red_science, int TimeGoalMin){ 

    // Assemblers (Red)
    float red_assembler = 1 / (assembler.craftspeed * 1/5 * 60);
    float gear_assembler = 1 / (assembler.craftspeed * 1/0.5 * 60);
    float total_red_assembler = red_assembler+gear_assembler;

    // Furnaces (Red)
    float iron_plate_furnace = 2 / (furnace.craftspeed * 1/3.2 * 60);
    float copper_plate_furnace = 1 / (furnace.craftspeed * 1/3.2 * 60);
    float total_red_furnace = iron_plate_furnace + copper_plate_furnace;

    // Miners (Red)
    float iron_miner = 2 / (miner.craftspeed * 60);
    float copper_miner = 1 / (miner.craftspeed * 60);
    float coal_miner = 0.108 / (miner.craftspeed * 60);
    float total_red_miner = iron_miner + copper_miner + coal_miner;

    // Power (Red)
    float total_building_power  = total_red_assembler * assembler.power;
          total_building_power += total_red_furnace * furnace.power;
          total_building_power += total_red_miner * miner.power;
    float boiler = total_building_power / (STEAM_ENGINE_POWER * ENGINE_PER_BOILER);
              
    // Pollution (Red)
    float pollution_per_minute  = total_red_assembler * assembler.pollution;
          pollution_per_minute += total_red_furnace * furnace.pollution;
          pollution_per_minute += total_red_miner * miner.pollution;
          pollution_per_minute += boiler * BOILER_POLLUTION;

    float red_per_minute = total_red_science / TimeGoalMin;
    float total_pollution = pollution_per_minute * red_per_minute;

    return total_pollution;
}

/*
 * The total pollution of researching green science.
 */
float ComputeGreenPollution(int total_green_science, int TimeGoalMin){ 

    // Assemblers (Green)
    float green_assembler = 1 / (assembler.craftspeed * 1/6 * 60);
    float inserter_assembler = 1 / (assembler.craftspeed * 1/0.5 * 60);
    float green_circuit_assembler = 1 / (assembler.craftspeed * 1/0.5 * 60);
    float wire_assembler = 3 / (assembler.craftspeed * 2/0.5 * 60);
    float belt_assembler = 1 / (assembler.craftspeed * 2/0.5 * 60);
    float gear_assembler = 1.5 / (assembler.craftspeed * 1/0.5 * 60);
    float total_green_assembler  = green_assembler+inserter_assembler+green_circuit_assembler;
          total_green_assembler += wire_assembler+belt_assembler+gear_assembler;

    // Furnaces (Green)
    float iron_plate_furnace = 5.5 / (furnace.craftspeed * 1/3.2 * 60);
    float copper_plate_furnace = 1.5 / (furnace.craftspeed * 1/3.2 * 60);
    float total_green_furnace = iron_plate_furnace + copper_plate_furnace;
     
    // Miners (Green)
    float iron_miner = 5.5 / (miner.craftspeed * 60);
    float copper_miner = 1.5 / (miner.craftspeed * 60);
    float coal_miner = 0.252 / (miner.craftspeed * 60);
    float total_green_miner = iron_miner + copper_miner + coal_miner;

    // Power (Green)
    float total_building_power  = total_green_assembler * assembler.power;
          total_building_power += total_green_furnace * furnace.power;
          total_building_power += total_green_miner * miner.power;
    float boiler = total_building_power / (STEAM_ENGINE_POWER * ENGINE_PER_BOILER);
              
    // Pollution (Green)
    float pollution_per_minute  = total_green_assembler * assembler.pollution;
          pollution_per_minute += total_green_furnace * furnace.pollution;
          pollution_per_minute += total_green_miner * miner.pollution;
          pollution_per_minute += boiler * BOILER_POLLUTION;

    float green_per_minute = total_green_science / TimeGoalMin;
    float total_pollution = pollution_per_minute * green_per_minute;
    
    return total_pollution;
}

float ComputeBluePollution(int total_blue_science, int TimeGoalMin){ 

    // Assemblers (Blue)
    float blue_assembler = 1 / (assembler.craftspeed * 2/24 * 60);
    float engine_assembler = 1 / (assembler.craftspeed * 1/10 * 60);
    float gear_assembler = 1 / (assembler.craftspeed * 1/0.5 * 60);
    float pipe_assembler = 2 / (assembler.craftspeed * 1/0.5 * 60);
    float red_circuit_assembler = 1.5 / (assembler.craftspeed * 1/6 * 60);
    float green_circuit_assembler = 3 / (assembler.craftspeed * 1/0.5 * 60);
    float wire_assembler = 15 / (assembler.craftspeed * 2/0.5 * 60);
    float total_blue_assembler  = blue_assembler+engine_assembler+gear_assembler+pipe_assembler;
          total_blue_assembler += red_circuit_assembler+green_circuit_assembler+wire_assembler;

    // Furnaces (Blue)
    float steel_furnace = 1 / (furnace.craftspeed * 1/16 * 60);
    float iron_plate_furnace = 12 / (furnace.craftspeed * 1/3.2 * 60);
    float copper_plate_furnace = 7.5 / (furnace.craftspeed * 1/3.2 * 60);
    float total_blue_furnace = steel_furnace + iron_plate_furnace + copper_plate_furnace;

    // Plants (Blue)
    float sulfur_plant  = 0.5 / (plant.craftspeed * 2/1 * 60);
    float total_blue_refinery = 37.5 / OIL_PETROL_AMOUNT;
    float heavy_plant = total_blue_refinery * OIL_HEAVY_RATIO;
    float light_plant = total_blue_refinery * OIL_LIGHT_RATIO;
    float total_blue_plant = sulfur_plant+heavy_plant+light_plant;
    
    // Miners (Blue)
    float iron_miner = 12 / (miner.craftspeed * 60);
    float copper_miner = 7.5 / (miner.craftspeed * 60);
    float coal_miner = 2.382 / (miner.craftspeed * 60);
    float total_blue_miner = iron_miner + copper_miner + coal_miner;
    
    // Power (Blue)
    float total_building_power  = total_blue_assembler * assembler.power;
          total_building_power += total_blue_furnace * furnace.power;
          total_building_power += total_blue_plant * plant.power;
          total_building_power += total_blue_refinery * refinery.power;
          total_building_power += total_blue_miner * miner.power;
    float boiler = total_building_power / (STEAM_ENGINE_POWER * ENGINE_PER_BOILER);
      
    // Pollution (Blue)
    float pollution_per_minute  = total_blue_assembler * assembler.pollution;
          pollution_per_minute += total_blue_furnace * furnace.pollution;
          pollution_per_minute += total_blue_plant * plant.pollution;
          pollution_per_minute += total_blue_refinery * refinery.pollution;
          pollution_per_minute += total_blue_miner * miner.pollution;
          pollution_per_minute += boiler * BOILER_POLLUTION;

    float blue_per_minute = total_blue_science / TimeGoalMin;
    float total_pollution = pollution_per_minute * blue_per_minute;

    return total_pollution;
}

float ComputePurplePollution(int total_purple_science, int TimeGoalMin){ 

    // Assemblers (Purple)
    float purple_assembler = 1 / (assembler.craftspeed * 3/21 * 60);
    float furnace_assembler = 0.333 / (assembler.craftspeed * 1/5 * 60);
    float T1P_assembler = 0.333 / (assembler.craftspeed * 1/15 * 60);
    float red_circuit_assembler = 3.333 / (assembler.craftspeed * 1/6 * 60);
    float green_circuit_assembler = 8.333 / (assembler.craftspeed * 1/0.5 * 60);
    float wire_assembler = 38.333 / (assembler.craftspeed * 2/0.5 * 60);
    float rail_assembler = 10 / (assembler.craftspeed * 2/0.5 * 60);
    float stick_assembler = 5 / (assembler.craftspeed * 2/0.5 * 60);
    float total_purple_assembler  = purple_assembler+furnace_assembler+T1P_assembler+red_circuit_assembler;
          total_purple_assembler += green_circuit_assembler+wire_assembler+rail_assembler+stick_assembler;     

    // Furnaces (Purple)
    float steel_furnace = 8.333 / (furnace.craftspeed * 1/16 * 60);
    float brick_furnace = 3.333 / (furnace.craftspeed * 1/3.2 * 60);
    float iron_plate_furnace = 52.5 / (furnace.craftspeed * 1/3.2 * 60);
    float copper_plate_furnace = 19.167 / (furnace.craftspeed * 1/3.2 * 60);
    float total_purple_furnace = brick_furnace + steel_furnace + iron_plate_furnace + copper_plate_furnace;

    // Plants (Purple)
    float plastic_plant  = 6.667 / (plant.craftspeed * 2/1 * 60);
    float total_purple_refinery = 66.667 / OIL_PETROL_AMOUNT;
    float heavy_plant = total_purple_refinery * OIL_HEAVY_RATIO;
    float light_plant = total_purple_refinery * OIL_LIGHT_RATIO;
    float total_purple_plant = plastic_plant+heavy_plant+light_plant;
    
    // Miners (Purple)
    float iron_miner = 52.5 / (miner.craftspeed * 60);
    float copper_miner = 19.167 / (miner.craftspeed * 60);
    float coal_miner = 7.533 / (miner.craftspeed * 60);
    float total_purple_miner = iron_miner + copper_miner + coal_miner;
    
    // Power (Purple)
    float total_building_power  = total_purple_assembler * assembler.power;
          total_building_power += total_purple_furnace * furnace.power;
          total_building_power += total_purple_plant * plant.power;
          total_building_power += total_purple_refinery * refinery.power;
          total_building_power += total_purple_miner * miner.power;
    float boiler = total_building_power / (STEAM_ENGINE_POWER * ENGINE_PER_BOILER);
      
    // Pollution (Purple)
    float pollution_per_minute  = total_purple_assembler * assembler.pollution;
          pollution_per_minute += total_purple_furnace * furnace.pollution;
          pollution_per_minute += total_purple_plant * plant.pollution;
          pollution_per_minute += total_purple_refinery * refinery.pollution;
          pollution_per_minute += total_purple_miner * miner.pollution;
          pollution_per_minute += boiler * BOILER_POLLUTION;

    float purple_per_minute = total_purple_science / TimeGoalMin;
    float total_pollution = pollution_per_minute * purple_per_minute;

    return total_pollution;
}

/*
 * The total pollution of researching solar panels and accumulators.
 */
float ComputeSolarResearchPollution(int TimeGoalMin){ 

    int i, total_red_science=0, total_green_science=0;

    // Red, Green, Blue, Purple science packs
    int SolarScienceCostMatrix[14][4] = {{10,  0,   0, 0},  // Automation
                                         {30,  0,   0, 0},  // Electronics
                                         {75,  0,   0, 0},  // Green science
                                         {50,  0,   0, 0},  // Steel
                                         {10,  0,   0, 0},  // Optics
                                         {40,  40,  0, 0},  // Automation2
                                         {50,  50,  0, 0},  // Fluids
                                         {100, 100, 0, 0},  // Engine
                                         {100, 100, 0, 0},  // Oil processing
                                         {120, 120, 0, 0},  // Power poles
                                         {150, 150, 0, 0},  // Sulfur
                                         {150, 150, 0, 0},  // Battery
                                         {150, 150, 0, 0},  // Accumulators
                                         {250, 250, 0, 0}}; // Solar panels
    for (i=0; i<14; i++){
        total_red_science   += SolarScienceCostMatrix[i][0];
        total_green_science += SolarScienceCostMatrix[i][1];
    }
    //printf("%d %d\n",total_red_science,total_green_science);
    
    float total_pollution  = ComputeRedPollution(total_red_science, TimeGoalMin);
          total_pollution += ComputeGreenPollution(total_green_science, TimeGoalMin);

    return total_pollution;
}

/*
 * The total pollution of researching nuclear technologies (centrifuges, reactors, reprocessing, kovarex).
 */
float ComputeNuclearResearchPollution(int TimeGoalMin){ 

    int i, total_red_science=0, total_green_science=0, total_blue_science=0, total_purple_science=0;

    // Red, Green, Blue, Purple science packs
    int NuclearScienceCostMatrix[30][4] = {{10,   0,    0,    0   },  // Automation
                                           {20,   0,    0,    0   },  // Yellow belts
                                           {30,   0,    0,    0   },  // Electronics
                                           {75,   0,    0,    0   },  // Green science
                                           {50,   0,    0,    0   },  // Steel
                                           {40,   40,   0,    0   },  // Automation2
                                           {50,   50,   0,    0   },  // Fluids
                                           {50,   50,   0,    0   },  // T1P
                                           {50,   50,   0,    0   },  // Flammable
                                           {75,   75,   0,    0   },  // Furnace 2
                                           {75,   75,   0,    0   },  // Blue science
                                           {75,   75,   0,    0   },  // Railway
                                           {100,  100,  0,    0   },  // Engine
                                           {100,  100,  0,    0   },  // Pumpjacks
                                           {100,  100,  0,    0   },  // Oil processing
                                           {100,  100,  0,    0   },  // Modules
                                           {120,  120,  0,    0   },  // Power poles
                                           {150,  150,  0,    0   },  // Sulfur
                                           {200,  200,  0,    0   },  // Plastics
                                           {200,  200,  0,    0   },  // Red belts
                                           {200,  200,  0,    0   },  // Red circuits
                                           {250,  250,  0,    0   },  // Concrete
                                           {75,   75,   75,   0   },  // Advanced oil
                                           {100,  100,  100,  0   },  // Purple science
                                           {200,  200,  200,  0   },  // Centrifuges
                                           {250,  250,  250,  0   },  // E-furnace
                                           {300,  300,  300,  0   },  // Rocket fuel
                                           {800,  800,  800,  0   },  // Nuclear reactors
                                           {50,   50,   50,   50  },  // Reprocessing
                                           {1500, 1500, 1500, 1500}}; // Motherfucking kovarex
    for (i=0; i<30; i++){
        total_red_science    += NuclearScienceCostMatrix[i][0];
        total_green_science  += NuclearScienceCostMatrix[i][1];
        total_blue_science   += NuclearScienceCostMatrix[i][2];
        total_purple_science += NuclearScienceCostMatrix[i][3];
    }
    //printf("%d %d %d %d\n",total_red_science,total_green_science,total_blue_science,total_purple_science);
    
    float total_pollution  = ComputeRedPollution(total_red_science, TimeGoalMin);
          total_pollution += ComputeGreenPollution(total_green_science, TimeGoalMin);
          total_pollution += ComputeBluePollution(total_blue_science, TimeGoalMin);
          total_pollution += ComputePurplePollution(total_purple_science, TimeGoalMin);

    return total_pollution;
}

/*
 * The total pollution of making enough solar panels to reach PowerGoalMW.
 * First, we compute the pollution of making 1 solar panel per minute.
 */
float ComputeSolarPanelPollution(int PowerGoalMW, int TimeGoalMin){ 

    // Assemblers
    float solar_assembler = 1 / (assembler.craftspeed * 1/10 * 60);
    float green_assembler = 15 / (assembler.craftspeed * 1/0.5 * 60);
    float cable_assembler = 45 / (assembler.craftspeed * 2/0.5 * 60);
    float total_solar_assembler = solar_assembler + green_assembler + cable_assembler;

    // Furnaces
    float steel_furnace = 5 / (furnace.craftspeed * 1/16 * 60);
    float iron_plate_furnace = 40 / (furnace.craftspeed * 1/3.2 * 60);
    float copper_plate_furnace = 27.5 / (furnace.craftspeed * 1/3.2 * 60);
    float total_solar_furnace = steel_furnace + iron_plate_furnace + copper_plate_furnace;

    // Miners
    float iron_miner = 40 / (miner.craftspeed * 60);
    float copper_miner = 27.5 / (miner.craftspeed * 60);
    float coal_miner = 3.33 / (miner.craftspeed * 60);
    float total_solar_miner = iron_miner + copper_miner + coal_miner;

    // Power
    float total_building_power  = total_solar_assembler * assembler.power;
          total_building_power += total_solar_furnace * furnace.power;
          total_building_power += total_solar_miner * miner.power;
    float boiler = total_building_power / (STEAM_ENGINE_POWER * ENGINE_PER_BOILER); 
 
    // Pollution
    float pollution_per_minute  = total_solar_assembler * assembler.pollution;
          pollution_per_minute += total_solar_furnace * furnace.pollution;
          pollution_per_minute += total_solar_miner * miner.pollution;
          pollution_per_minute += boiler * BOILER_POLLUTION;

    float solar_panel_per_minute = SOLAR_PANEL_RATIO * (PowerGoalMW / SOLAR_PANEL_POWER) / TimeGoalMin;
    float total_pollution = pollution_per_minute * solar_panel_per_minute;

    return total_pollution;
}

/*
 * The total pollution of making enough accumulators to reach PowerGoalMW.
 */
float ComputeAccumulatorPollution(int PowerGoalMW, int TimeGoalMin){ 

    // Assemblers
    float total_accumulator_assembler = 1 / (assembler.craftspeed * 1/10 * 60);

    // Furnaces
    float iron_plate_furnace = 9 / (furnace.craftspeed * 1/3.2 * 60);
    float copper_plate_furnace = 5 / (furnace.craftspeed * 1/3.2 * 60);
    float total_accumulator_furnace = iron_plate_furnace + copper_plate_furnace;
    
    // Plants
    float battery_plant = 5 / (plant.craftspeed * 1/4 * 60);
    float acid_plant    = 100 / (plant.craftspeed * 50/1 * 60);
    float sulfur_plant  = 10 / (plant.craftspeed * 2/1 * 60);
    float total_accumulator_refinery = 150 / OIL_PETROL_AMOUNT;
    float heavy_plant = total_accumulator_refinery * OIL_HEAVY_RATIO;
    float light_plant = total_accumulator_refinery * OIL_LIGHT_RATIO;
    float total_accumulator_plant = battery_plant+acid_plant+sulfur_plant+heavy_plant+light_plant;
    
    // Miners
    float iron_miner = 9 / (miner.craftspeed * 60);
    float copper_miner = 5 / (miner.craftspeed * 60);
    float coal_miner = 0.504 / (miner.craftspeed * 60);
    float total_accumulator_miner = iron_miner + copper_miner + coal_miner;
    
    // Power
    float total_building_power  = total_accumulator_assembler * assembler.power;
          total_building_power += total_accumulator_furnace * furnace.power;
          total_building_power += total_accumulator_plant * plant.power;
          total_building_power += total_accumulator_refinery * refinery.power;
          total_building_power += total_accumulator_miner * miner.power;
    float boiler = total_building_power / (STEAM_ENGINE_POWER * ENGINE_PER_BOILER);
      
    // Pollution
    float pollution_per_minute  = total_accumulator_assembler * assembler.pollution;
          pollution_per_minute += total_accumulator_furnace * furnace.pollution;
          pollution_per_minute += total_accumulator_plant * plant.pollution;
          pollution_per_minute += total_accumulator_refinery * refinery.pollution;
          pollution_per_minute += total_accumulator_miner * miner.pollution;
          pollution_per_minute += boiler * BOILER_POLLUTION;

    float accumulator_per_minute = ACCUMULATOR_RATIO * SOLAR_PANEL_RATIO * (PowerGoalMW / SOLAR_PANEL_POWER) / TimeGoalMin;
    float total_pollution = pollution_per_minute * accumulator_per_minute;

    return total_pollution;
}

/*
 * We can compute the minimum nuclear infrastructure required for a target power.
 * For example, a target of 480MW is met with 4 reactors, 48 exchangers, 82.4 turbines, 69.6 heatpipes.
 */
float ComputeMinimalReactorSetup(int PowerGoalMW){ 

    power=0;

    for (reactor=1; power < PowerGoalMW; reactor++){
        edge = fmaxf(0, (3 * reactor / 2) - 2 - ((reactor % 2) / 2) );
        exchanger = (edge * 2 + reactor) * 4;
        heatpipe = exchanger * HEATPIPE_RATIO;
        turbine = exchanger * STEAM_RATIO;
        power = exchanger * EXCHANGER_POWER;
        if (power >= PowerGoalMW) break; // Jank
    }
    return 0;
}

/*
 * The total pollution of making enough accumulators to reach PowerGoalMW.
 */
float ComputeReactorPollution(int TimeGoalMin){ 

    // Assemblers (Reactor)
    float reactor_assembler = 1 / (assembler.craftspeed * 1/8 * 60);
    float concrete_assembler = 500 / (assembler.craftspeed * 10/10 * 60);
    float red_circuit_assembler = 500 / (assembler.craftspeed * 1/6 * 60);
    float green_circuit_assembler = 1000 / (assembler.craftspeed * 1/0.5 * 60);
    float wire_assembler = 5000 / (assembler.craftspeed * 2/0.5 * 60);
    float total_reactor_assembler  = reactor_assembler+concrete_assembler+red_circuit_assembler;
          total_reactor_assembler += green_circuit_assembler+wire_assembler;

    // Furnaces (Reactor)
    float steel_furnace = 500 / (furnace.craftspeed * 1/16 * 60);
    float brick_furnace = 250 / (furnace.craftspeed * 1/3.2 * 60);
    float iron_plate_furnace = 3500 / (furnace.craftspeed * 1/3.2 * 60);
    float copper_plate_furnace = 3000 / (furnace.craftspeed * 1/3.2 * 60);
    float total_reactor_furnace = steel_furnace + brick_furnace + iron_plate_furnace + copper_plate_furnace;
    
    // Plants (Reactor)
    float plastic_plant = 1000 / (plant.craftspeed * 2/1 * 60);
    float total_reactor_refinery = 10000 / OIL_PETROL_AMOUNT;
    float heavy_plant = total_reactor_refinery * OIL_HEAVY_RATIO;
    float light_plant = total_reactor_refinery * OIL_LIGHT_RATIO;
    float total_reactor_plant = plastic_plant+heavy_plant+light_plant;
    
    // Miners (Reactor)
    float iron_miner = 3550 / (miner.craftspeed * 60);
    float copper_miner = 3000 / (miner.craftspeed * 60);
    float coal_miner = 833 / (miner.craftspeed * 60);
    float stone_miner = 500 / (miner.craftspeed * 60);
    float total_reactor_miner = iron_miner + copper_miner + coal_miner + stone_miner;
    
    // Power (Reactor)
    float total_building_power  = total_reactor_assembler * assembler.power;
          total_building_power += total_reactor_furnace * furnace.power;
          total_building_power += total_reactor_plant * plant.power;
          total_building_power += total_reactor_refinery * refinery.power;
          total_building_power += total_reactor_miner * miner.power;
    float boiler = total_building_power / (STEAM_ENGINE_POWER * ENGINE_PER_BOILER);
      
    // Pollution (Reactor)
    float pollution_per_minute  = total_reactor_assembler * assembler.pollution;
          pollution_per_minute += total_reactor_furnace * furnace.pollution;
          pollution_per_minute += total_reactor_plant * plant.pollution;
          pollution_per_minute += total_reactor_refinery * refinery.pollution;
          pollution_per_minute += total_reactor_miner * miner.pollution;
          pollution_per_minute += boiler * BOILER_POLLUTION;

    float reactor_per_minute = reactor / TimeGoalMin;
    float total_pollution = pollution_per_minute * reactor_per_minute;

    return total_pollution;
}

/*
 * The total pollution of making enough heat pipes to reach PowerGoalMW.
 * First, we compute the pollution of making 1 heat pipe per minute.
 */
float ComputeHeatpipePollution(int TimeGoalMin){ 

    // Assemblers
    float total_heatpipe_assembler = 1 / (assembler.craftspeed * 1/10 * 60);

    // Furnaces
    float steel_furnace = 10 / (furnace.craftspeed * 1/16 * 60);
    float iron_plate_furnace = 50 / (furnace.craftspeed * 1/3.2 * 60);
    float copper_plate_furnace = 20 / (furnace.craftspeed * 1/3.2 * 60);
    float total_heatpipe_furnace = steel_furnace + iron_plate_furnace + copper_plate_furnace;

    // Miners
    float iron_miner = 50 / (miner.craftspeed * 60);
    float copper_miner = 20 / (miner.craftspeed * 60);
    float coal_miner = 4.32 / (miner.craftspeed * 60);
    float total_heatpipe_miner = iron_miner + copper_miner + coal_miner;

    // Power
    float total_building_power  = total_heatpipe_assembler * assembler.power;
          total_building_power += total_heatpipe_furnace * furnace.power;
          total_building_power += total_heatpipe_miner * miner.power;
    float boiler = total_building_power / (STEAM_ENGINE_POWER * ENGINE_PER_BOILER);      
 
    // Pollution
    float pollution_per_minute  = total_heatpipe_assembler * assembler.pollution;
          pollution_per_minute += total_heatpipe_furnace * furnace.pollution;
          pollution_per_minute += total_heatpipe_miner * miner.pollution;
          pollution_per_minute += boiler * BOILER_POLLUTION;

    float heatpipe_per_minute = heatpipe / TimeGoalMin;
    float total_pollution = pollution_per_minute * heatpipe_per_minute;

    return total_pollution;
}

/*
 * The total pollution of making enough exchangers to reach PowerGoalMW.
 * First, we compute the pollution of making 1 exchanger per minute.
 */
float ComputeExchangerPollution(int TimeGoalMin){ 

    // Assemblers
    float total_exchanger_assembler = 1 / (assembler.craftspeed * 1/3 * 60);

    // Furnaces
    float steel_furnace = 10 / (furnace.craftspeed * 1/16 * 60);
    float iron_plate_furnace = 60 / (furnace.craftspeed * 1/3.2 * 60);
    float copper_plate_furnace = 100 / (furnace.craftspeed * 1/3.2 * 60);
    float total_exchanger_furnace = steel_furnace + iron_plate_furnace + copper_plate_furnace;

    // Miners
    float iron_miner = 60 / (miner.craftspeed * 60);
    float copper_miner = 100 / (miner.craftspeed * 60);
    float coal_miner = 7.56 / (miner.craftspeed * 60);
    float total_exchanger_miner = iron_miner + copper_miner + coal_miner;

    // Power
    float total_building_power  = total_exchanger_assembler * assembler.power;
          total_building_power += total_exchanger_furnace * furnace.power;
          total_building_power += total_exchanger_miner * miner.power;
    float boiler = total_building_power / (STEAM_ENGINE_POWER * ENGINE_PER_BOILER);      
 
    // Pollution
    float pollution_per_minute  = total_exchanger_assembler * assembler.pollution;
          pollution_per_minute += total_exchanger_furnace * furnace.pollution;
          pollution_per_minute += total_exchanger_miner * miner.pollution;
          pollution_per_minute += boiler * BOILER_POLLUTION;

    float exchanger_per_minute = exchanger / TimeGoalMin;
    float total_pollution = pollution_per_minute * exchanger_per_minute;

    return total_pollution;
}

/*
 * The total pollution of making enough turbines to reach PowerGoalMW.
 * First, we compute the pollution of making 1 turbine per minute.
 */
float ComputeTurbinePollution(int TimeGoalMin){ 

    // Assemblers (Turbine)
    float turbine_assembler = 1 / (assembler.craftspeed * 1/3 * 60);
    float gear_assembler = 50 / (assembler.craftspeed * 1/0.5 * 60);
    float pipe_assembler = 20 / (assembler.craftspeed * 1/0.5 * 60);
    float total_turbine_assembler = turbine_assembler+gear_assembler+pipe_assembler;

    // Furnaces (Turbine)
    float iron_plate_furnace = 120 / (furnace.craftspeed * 1/3.2 * 60);
    float copper_plate_furnace = 50 / (furnace.craftspeed * 1/3.2 * 60);
    float total_turbine_furnace = iron_plate_furnace + copper_plate_furnace;

    // Miners (Turbine)
    float iron_miner = 120 / (miner.craftspeed * 60);
    float copper_miner = 50 / (miner.craftspeed * 60);
    float coal_miner = 6.12 / (miner.craftspeed * 60);
    float total_turbine_miner = iron_miner + copper_miner + coal_miner;

    // Power (Turbine)
    float total_building_power  = total_turbine_assembler * assembler.power;
          total_building_power += total_turbine_furnace * furnace.power;
          total_building_power += total_turbine_miner * miner.power;
    float boiler = total_building_power / (STEAM_ENGINE_POWER * ENGINE_PER_BOILER);      
 
    // Pollution (Turbine)
    float pollution_per_minute  = total_turbine_assembler * assembler.pollution;
          pollution_per_minute += total_turbine_furnace * furnace.pollution;
          pollution_per_minute += total_turbine_miner * miner.pollution;
          pollution_per_minute += boiler * BOILER_POLLUTION;

    float turbine_per_minute = turbine / TimeGoalMin;
    float total_pollution = pollution_per_minute * turbine_per_minute;

    return total_pollution;
}

/*
 * The pollution per minute of processing enough fuel cells to reach PowerGoalMW.
 * First, we compute the pollution per minute to feed one reactor.
 */
float ComputeNuclearPollutionPerMinute(void){ 

    // Assemblers
    float total_fuelcell_assembler = 0.3 / (assembler.craftspeed * 10/10 * 60);

    // Centrifuges
    float uranium_centrifuge = 0.697 / (centrifuge.craftspeed * 0.993/12 * 60);
    float reprocessing_centrifuge = 0.3 / (centrifuge.craftspeed * 5/60 * 60);
    float kovarex_centrifuge = 0.03 / (centrifuge.craftspeed * 1/60 * 60);
    float total_fuelcell_centrifuge = uranium_centrifuge+reprocessing_centrifuge+kovarex_centrifuge;

    // Furnaces
    float total_fuelcell_furnace = 0.43 / (furnace.craftspeed * 1/3.2 * 60);
    
    // Plants
    float acid_plant = 6.509 / (plant.craftspeed * 50/1 * 60);
    float sulfur_plant = 0.651 / (plant.craftspeed * 2/1 * 60);
    float total_fuelcell_refinery = 9.763 / OIL_PETROL_AMOUNT;
    float heavy_plant = total_fuelcell_refinery * OIL_HEAVY_RATIO;
    float light_plant = total_fuelcell_refinery * OIL_LIGHT_RATIO;
    float total_fuelcell_plant = acid_plant+sulfur_plant+heavy_plant+light_plant;
    
    // Miners
    float iron_miner = 0.43 / (miner.craftspeed * 60);
    float coal_miner = 0.015 / (miner.craftspeed * 60);
    float uranium_miner = 6.509 / (miner.craftspeed / 2 * 60);
    float total_fuelcell_miner = iron_miner + coal_miner + uranium_miner;
    
    // Power
    /* Assumption : the base is then running on nuclear power, therefore no coal boiler pollution.
    float total_building_power  = total_fuelcell_assembler * assembler.power;
          total_building_power += total_fuelcell_centrifuge * centrifuge.power;
          total_building_power += total_fuelcell_furnace * furnace.power;
          total_building_power += total_fuelcell_plant * plant.power;
          total_building_power += total_fuelcell_refinery * refinery.power;
          total_building_power += total_fuelcell_miner * miner.power;
    float boiler = total_building_power / (STEAM_ENGINE_POWER * ENGINE_PER_BOILER);
    */
      
    // Pollution
    float pollution_per_minute  = total_fuelcell_assembler * assembler.pollution;
          pollution_per_minute += total_fuelcell_centrifuge * centrifuge.pollution;
          pollution_per_minute += total_fuelcell_furnace * furnace.pollution;
          pollution_per_minute += total_fuelcell_plant * plant.pollution;
          pollution_per_minute += total_fuelcell_refinery * refinery.pollution;
          pollution_per_minute += total_fuelcell_miner * miner.pollution;
          //pollution_per_minute += boiler * BOILER_POLLUTION;
    
    return pollution_per_minute * reactor;
}

/*
 * Generation of output.csv file.
 */
void GenerateCsvFile(void){

    int power1, power2, TimeGoalMin, PowerGoalMW;
    float SolarResearchPollution, SolarProductionPollution, TotalSolarPollution;
    float NuclearResearchPollution, NuclearProductionPollution, TotalNuclearPollution;
    float NuclearPollutionCatchUpTimeHour;

    // Graph setup
	printf("Min Target Power (100MW) "); scanf("%d", &power1); 
	printf("Max Target Power (150MW) "); scanf("%d", &power2);
	printf("TimeGoalMin      (60min) "); scanf("%d", &TimeGoalMin);
    
    // Remove old output and open a new output.csv
    remove("output.csv");
    FILE *fptr;
    fptr = fopen("output.csv", "a");

    // Constant pollution emitted during research
    SolarResearchPollution   = ComputeSolarResearchPollution(TimeGoalMin);
    NuclearResearchPollution = ComputeNuclearResearchPollution(TimeGoalMin);

	// Variable pollution emitted during production
    for (PowerGoalMW = power1; PowerGoalMW <= power2; PowerGoalMW++) {

        SolarProductionPollution  = ComputeSolarPanelPollution(PowerGoalMW, TimeGoalMin);
        SolarProductionPollution += ComputeAccumulatorPollution(PowerGoalMW, TimeGoalMin);
        TotalSolarPollution = SolarResearchPollution + SolarProductionPollution;

        ComputeMinimalReactorSetup(PowerGoalMW);
        NuclearProductionPollution  = ComputeReactorPollution(TimeGoalMin);
        NuclearProductionPollution += ComputeHeatpipePollution(TimeGoalMin);
        NuclearProductionPollution += ComputeExchangerPollution(TimeGoalMin);
        NuclearProductionPollution += ComputeTurbinePollution(TimeGoalMin);
        TotalNuclearPollution = NuclearResearchPollution + NuclearProductionPollution;

        NuclearPollutionCatchUpTimeHour  = (TotalSolarPollution - TotalNuclearPollution);
        NuclearPollutionCatchUpTimeHour /= ComputeNuclearPollutionPerMinute();
        NuclearPollutionCatchUpTimeHour /= 60.0;
        if (NuclearPollutionCatchUpTimeHour < 0) NuclearPollutionCatchUpTimeHour = 0;

        fprintf(fptr, "%d,",  PowerGoalMW);
        fprintf(fptr, "%f,",  TotalNuclearPollution);
        fprintf(fptr, "%f,",  NuclearResearchPollution);
        fprintf(fptr, "%f,",  TotalSolarPollution);
        fprintf(fptr, "%f,",  SolarResearchPollution);
        fprintf(fptr, "%f\n", NuclearPollutionCatchUpTimeHour);
    }
    fclose(fptr); 
}

int main(void){

    GenerateCsvFile();

    return 0;
}
