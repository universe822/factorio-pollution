#!/usr/bin/gnuplot

reset

# input .csv separator beween values
set datafile separator ","

# output
set terminal pngcairo size 1080,1080
set output "graph.png"
set multiplot layout 1,2 title 'Pollution : solar power or nuclear power?'
                     
# key (legend), opaque=display a white screen behind the legend to make it easier to read
set key opaque
#set key outside Left

# Text of first plot
set title  "Power threshold at which nuclear power is more pollution-efficient"
set xlabel "Power Infrastructure (MW)"
set ylabel "Total pollution"

# tics of first plot
set tics front
#set xtics 40
#set ytics 1000

# Removes weird useless white space added on the right of values when using filledcurves
set autoscale fix

# Avoid weird y autorange, can be anything but starts at 0
set yrange[0:*];

# u=using, X:Y=draw the Y column over X column, ti=title,  w=with, filledc=filledcurves, x1=fill everything between abcissa (x1) and selected curve.
# ,\=everything before and after it is a single line command, useful to plot several curves with one "plot".
# fs=fillstyle, solid=one solid color, .5=opacity 50%, transparent pattern 4=a type of fillstyle where the surface is filled with parralel lines.
# lc=linecolor

plot 'output.csv' u 1:2 ti 'Pollution from nuclear production' w filledc x1 fs solid                 lc "dark-green",\
     'output.csv' u 1:3 ti 'Pollution from nuclear research'   w filledc x1 fs solid .5              lc "web-green" ,\
     'output.csv' u 1:4 ti 'Pollution from solar production'   w filledc x1 fs transparent pattern 4 lc "web-blue"  ,\
     'output.csv' u 1:4 notitle                                w lines                               lc "web-blue"  ,\
     'output.csv' u 1:5 ti 'Pollution from solar research'     w filledc x1 fs solid                 lc "web-blue" 

# Text of second plot
set title  "Time it takes for the pollution from fuel cell production to catch up to solar pollution"
set xlabel "Power Infrastructure (MW)"
set ylabel "Time (Hour)"

# tics of second plot
#set ytics 10

plot 'output.csv' u 1:6 ti 'Time (Hour)'                       w lines                               lc "dark-red"

# Close the usual gnuplot window on mouse click, useless if we generate a png directly
#pause mouse

