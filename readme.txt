
Pollution comparison of solar vs. nuclear power.

I compare hypothetical factorio bases by measuring their pollution, outputing pollution values in a .csv and drawing a comparative graph using gnuplot.
UPS, resource, player time, and fun are all valuable, here I take pollution optimization to the extreme, for fun. 
This is a simplistic comparison that doesn't take productivity and modules into acount. WIP

First, compile and execute pollution.c : choose the limits of the X axis and the production/research time.
Then, execute "graph.gnu" in order to generate "graph.png".
Open graph.png, Done.
--------------------------------------------------------------------------------

Show inputs ans outputs of the program here WIP
--------------------------------------------------------------------------------

For every MW, there are four bases :
The solar production base will make S solar panels and A accumulators in a time T, depending on P the power you need (ie. 1-480MW).
The solar research base will research all solar technologies in the same time T.
The nuclear production base will make R reactors, E exchangers, H heat pipes, etc... in the same time T to build the equivalent infrastructure (the same power P).
The nuclear research base will research all nuclear technologies in the same time T. 
The actual time T is pretty irrelevant : it's only useful to compare the two infrastructures.

Nuclear research include reprocessing and kovarex because I don't consider a setup where you stock u238 or spent fuel cells indefinitelly a true nuclear setup.
So I add up the pollution from research and production, for both, and I compare the two. 

There is a function to compute how much time it would take for the nuclear infrastructure to pollute as much as the solar infrastructure. 
There are lots of assumptions, but I think it's fair enough to really compare the two.

The steps you see on the nuclear production curve just means the base needs one more reactor and reactor accessories to output the power P. See results folder.
--------------------------------------------------------------------------------

Assumptions :
 - Most recipe values are taken directly from the kirkmacdonald.io calculator.
 - Both nuclear and solar infrastructure are made using coal power.
 - I ignore the initial pollution cost of producing the first entities.
 - I ignore the pollution cost of making trains, pumps, power poles, pipes, inserters, etc..
 - I ignore the minimum power consumption from assemblers, plants, etc.
 - I ignore water as a resource.
 - I ignore the power required from pumpjacks.
 - Polluting entities : miners, furnaces, boilers, assemblers, pumpjacks, refineries, chemical plants, centrifuges.

Thank you again math-anon for the nuclear reactor shared-edge equation. I used it to compute the minimal nuclear setup for a set power. Very useful.

